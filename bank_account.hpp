#pragma once

#include <iostream>
#include <vector>

#include "transaction.hpp"

namespace project
{
  class person;
  class date;
}

namespace bank
{
  class bank_account
  {
  private:
    static constexpr unsigned int INITIAL_ACCOUNT_NUMBER = 1;

  protected:
    std::vector<transaction> transactions;
    std::string account_number, password;
    double account_balance;

  public:
    bank_account();
    bank_account(const std::string& password_,
                 double opening_balance = 0,
                 int offset = 0);
    bank_account(const bank_account& b_a);
    bank_account(bank_account&& b_a);

    bank_account& operator=(bank_account b_a);
    bank_account& operator=(bank_account&& b_a);

    void withdraw(double amt);
    void deposit(double amt);
    void change_password(const std::string& password_);

    void print_details() const;

    inline std::string get_account_number() const { return account_number; }
    inline double get_account_balance() const { return account_balance; }

    bool operator==(const bank_account& b_a) const;

    bool verify_password(const std::string& password_) const;

    friend void swap(bank_account& b1, bank_account& b2);
    friend std::istream& operator>>(std::istream& is, bank_account& b_a);
    friend std::ostream& operator<<(std::ostream& os, const bank_account& b_a);
  };
}
