#pragma once

#include <string>

#include "bank_account.hpp"
#include "date.hpp"

namespace project
{
  class person
  {
  private:
    std::string first_name, last_name, account_number;
    date date_of_birth;
    bank::bank_account account;

  public:
    person(const std::string& f_name = "",
           const std::string& l_name = "",
           int d = 0, int m = 0, int y = 0);
    person(const std::string& f_name,
           const std::string& l_name,
           date dob);
    person(const std::string& f_name,
           const std::string& l_name,
           int d, int m, int y,
           const bank::bank_account& b_a);
    person(const std::string& f_name,
           const std::string& l_name,
           date dob,
           const bank::bank_account& b_a);

    inline std::string get_account_number() const { return account_number; }
    inline std::string get_name() const { return first_name + " " + last_name; }
    inline date get_date_of_birth() const { return date_of_birth; }
    inline bank::bank_account get_account() const { return account; }

    void set_account(const bank::bank_account& b_a);

    void save_account() const;

    void print_details() const;

    operator bool();

    friend std::ostream& operator<<(std::ostream& os, const person& p);
    friend std::istream& operator>>(std::istream& is, person& p);
  };
}
