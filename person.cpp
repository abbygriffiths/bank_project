#include <fstream>

#include "person.hpp"

namespace project
{
  person::person(const std::string& f_name,
                 const std::string& l_name,
                 int d, int m, int y):
    person(f_name, l_name, date(d, m, y))
  {
  }

  person::person(const std::string& f_name,
                 const std::string& l_name,
                 date dob):
    person(f_name, l_name, dob, bank::bank_account {})
  {
  }

  person::person(const std::string& f_name,
                 const std::string& l_name,
                 int d, int m, int y,
                 const bank::bank_account& b_a):
    person(f_name, l_name, date(d, m, y), b_a)
  {
  }

  person::person(const std::string& f_name,
                 const std::string& l_name,
                 date dob,
                 const bank::bank_account& b_a):
    first_name(f_name),
    last_name(l_name),
    account_number(b_a.get_account_number()),
    date_of_birth(dob),
    account(b_a)
  {
  }

  void person::set_account(const bank::bank_account& b_a)
  {
    account = b_a;
    account_number = account.get_account_number();
  }

  void person::print_details() const
  {
    std::cout << "Name: " << first_name << " " << last_name << std::endl
              << "Date of Birth: " << date_of_birth << std::endl
              << "Account Details: ";
    account.print_details();
  }

  void person::save_account() const
  {
    std::fstream os(".accounts.dat", std::ios::binary|std::ios::out|std::ios::app);

    bank::bank_account acc;

    // check if file opened correctly
    if (os)
      {
        while (! os.eof())
          {
            os.read(reinterpret_cast<char*>(&acc), sizeof(acc));

            // if account already exists in records
            if (acc.get_account_number() == account_number)
              {
                os.seekp(-(sizeof(bank::bank_account)), std::ios_base::cur);
                break;
              }
          }
        // if it somehow doesnt exist in records
        os.write(reinterpret_cast<const char*>(&account), sizeof(account));
      }

    os.close();
  }

  person::operator bool()
  {
    return !get_name().empty();
  }

  std::ostream& operator<<(std::ostream& os, const person& p)
  {
    os << p.first_name << " "
       << p.last_name << " "
       << p.account_number << std::endl;
    os << p.date_of_birth << std::endl;
    os << p.account << std::endl;

    return os;
  }

  std::istream& operator>>(std::istream& is, person& p)
  {
    is >> p.first_name >> p.last_name >> p.account_number
       >> p.date_of_birth
       >> p.account;
    return is;
  }
}

