#include <cstdlib> // for std::exit
#include <fstream> // for file handling
#include <iostream> // for IO to the console
#include <string> // for std::string class
#include <vector> // for std::vector class

#include "bank_account.hpp"
#include "person.hpp"
#include "user_interface.hpp"

namespace project
{
  const std::string users_file_name = ".users.proj.dat";

  // file object to interact with stored users
  std::fstream users_file;

  // file object to interact with stored accounts
  std::fstream accounts_file;

  void show_main_menu()
  {
    int choice = 0;

    std::system("clear");

    // Show bank header
    print_separator();
    std::cout << "\t\t\t\tAAA Bank" << std::endl;
    print_separator();

    // Show all the choices on main menu
    std::cout << "1. Open new account" << std::endl
              << "2. Login to existing account" << std::endl
              << "3. Exit" << std::endl
              << "Enter your choice: ";
    std::cin >> choice;

    // decide menu to show based on input choice
    if (choice == 1)
      {
        show_sign_up_menu();
      }
    else if (choice == 2)
      {
        show_login_menu();
      }
    else if (choice == 3)
      {
        std::exit(0);
      }
  }

  // Function that is used to take account number and password and redirect to account page
  void show_login_menu()
  {
    std::string acc_number, password;

    std::cout << "Enter your account number: ";
    std::cin >> acc_number;

    // function that gets password by turning off echo in the console
    password = get_password();

    person user = search_user(acc_number);

    if (user.get_account().verify_password(password))
      {
        show_account_menu(user);
      }
    else
      {
        std::cerr << "Incorrect password or account not found. Please try again." << std::endl;
        getch();
        show_main_menu();
      }
  }

  void show_sign_up_menu()
  {
    std::system("clear");

    // variables to be passed to account constructor
    std::string first_name = "", last_name = "";
    std::string password = "", password_confirmation = "";
    int d = 0, m = 0, y = 0;
    double starting_balance = 0;

    // take in input
    std::cout << "First Name: ";
    std::cin >> first_name;
    std::cout << "Last Name: ";
    std::cin >> last_name;
    std::cout << "Date of Birth (dd mm yyyy): ";
    std::cin >> d >> m >> y;
    std::cout << "Starting Balance (min. 500): ";
    std::cin >> starting_balance;

    password = get_password("Password: ");
    password_confirmation = get_password("Repeat password: ");

    if (password_confirmation == password)
      {
        // create bank account with offset of no. of accounts already in file
        int offset = count_accounts();
        users_file.open(users_file_name.c_str(), std::ios::out|std::ios::app);

        project::person user(first_name, last_name, project::date(d, m, y),
                             bank::bank_account(password, starting_balance, offset));

        users_file << user << std::endl;
        users_file.close();

        std::cout << "Welcome, " << first_name << " " << last_name << "! Your account number is " << user.get_account().get_account_number() << std::endl;
        getch();

        show_main_menu();
      }
    else
      {
        std::cerr << "There was a problem with the server. Please try again later." << std::endl;
        getch();
      }
  }

  void show_account_menu(const person& user)
  {
    std::cout << "Welcome, " << user.get_name() << std::endl;

    int choice = 0;

    std::cout << "1. Deposit" << std::endl
              << "2. Withdraw" << std::endl
              << "3. Log Out" << std::endl
              << "4. Delete account" << std::endl
              << "5. Change Password" << std::endl
              << "6. Print details" << std::endl
              << "Enter your choice: ";
    std::cin >> choice;

    if (choice == 1)
      {
        double amt = 0;
        std::cout << "Enter amount: ";
        std::cin >> amt;

        std::string password = get_password("Confirm password: ");
        if (user.get_account().verify_password(password))
          {
            user.get_account().deposit(amt);
            save_user(user);
          }
        else
          {
            std::cerr << "Password is incorrect. Please try again" << std::endl;
            getch();

            show_account_menu(user);
          }
      }
    else if (choice == 2)
      {
        double amt = 0;
        std::cout << "Enter amount: ";
        std::cin >> amt;

        std::string password = get_password("Confirm password: ");
        if (user.get_account().verify_password(password))
          {
            user.get_account().withdraw(amt);
            save_user(user);
          }
        else
          {
            std::cerr << "Password is incorrect. Please try again" << std::endl;
            getch();

            show_account_menu(user);
          }
      }
    else if (choice == 3)
      {
        show_main_menu();
      }
    else if (choice == 4)
      {
        char choice = 'n';
        std::cout << "Confirm deletion of account? (y/n): ";
        std::cin >> choice;

        if (choice == 'y' || choice == 'Y')
          {
            delete_user(user);
          }
        else
          {
            show_account_menu(user);
          }
      }
    else if (choice == 5)
      {
        std::string old_pass = "", new_pass = "", new_pass_confirmation = "";
        old_pass = get_password("Confirm old password: ");
        if (user.get_account().verify_password(old_pass))
          {
            new_pass = get_password("Enter new password: ");
            new_pass_confirmation = get_password("Confirm new password: ");

            if (new_pass_confirmation == new_pass)
              {
                user.get_account().change_password(new_pass);
              }
            else
              {
                std::cerr << "New password and confirmation don't match" << std::endl;
              }
          }

        save_user(user);
      }
    else if (choice == 6)
      {
        user.print_details();
        getch();

        show_account_menu(user);
      }
  }

  void print_separator()
  {
    for (int i = 0; i < 80; i++)
      {
        std::cout << "_";
      }
    std::cout << std::endl << std::endl;
  }

  std::string get_password(const std::string& prompt)
  {
    std::cout << prompt;

    std::string password = "";

    std::system("stty -echo");
    std::cin >> password;
    std::system("stty echo");
    std::cout << std::endl;

    return password;
  }

  void save_user(const project::person& user)
  {
    users_file.open(users_file_name.c_str(), std::ios::in);

    person temp;
    std::vector<person> users;

    while ((users_file >> temp) && temp)
      {
        std::cout << "TEST (reading file): " << temp.get_name() << std::endl;
        if (temp.get_account().get_account_number() ==
            user.get_account().get_account_number())
          {
            users.push_back(user);
          }
        else
          {
            users.push_back(temp);
          }
      }
    users_file.close();

    users_file.open(users_file_name.c_str(), std::ios::out|std::ios::trunc);

    for (const person& p : users)
      {
        users_file << p << std::endl;
      }
    users_file.close();
  }

  void delete_user(const project::person& user)
  {
    users_file.open(users_file_name.c_str(), std::ios::in);

    if (users_file)
      {
        person temp;
        std::vector<person> users;

        while (users_file)
          {
            std::cout << "TEST (looping): " << temp.get_name() << std::endl;
            if (temp.get_account().get_account_number() ==
                user.get_account().get_account_number())
              {
                continue;
              }
            else
              {
                users.push_back(temp);
              }
          }

        users_file.close();

        users_file.open(users_file_name.c_str(), std::ios::out|std::ios::trunc);

        for (const person& p : users)
          {
            users_file << p << std::endl;
          }
        users_file.close();
      }
  }

  person search_user(const std::string& acc_number)
  {
    users_file.open(users_file_name.c_str(), std::ios::in);

    if (users_file)
      {
        person user;
        while (users_file >> user)
          {
            if (user.get_account_number() == acc_number)
              {
                users_file.close();
                return user;
              }
          }
      }

    users_file.close();
    return person{}; // return default person class
  }

  int count_accounts()
  {
    int ctr = 0;
    users_file.open(users_file_name.c_str(), std::ios::in);

    std::string line = "";

    while (std::getline(users_file, line)) // FIXME: infinite loop
      {
        ctr++;
      }

    users_file.close();
    return ctr / 5;
  }
}
