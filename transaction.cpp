#include <iostream>
#include <map>

#include "transaction.hpp"

namespace bank
{
  transaction::transaction():
    amount(0),
    transaction_type(type::Default)
  {
  }

  transaction::transaction(double amt, type type_):
    amount(amt),
    transaction_type(type_)
  {
  }

  // function that allows usage of >> operator to read from a file
  std::istream& operator>>(std::istream& is, transaction& t)
  {
    is >> t.amount;

    std::string t_type = "";
    is >> t_type;

    std::map<std::string, transaction::type> type_map;
    type_map["Deposit"] = transaction::type::Deposit;
    type_map["Withdrawal"] = transaction::type::Withdrawal;

    t.transaction_type = type_map[t_type];

    return is;
  }

  // function that allows using << operator to write to files or console
  std::ostream& operator<<(std::ostream& os, const transaction& t)
  {
    std::map<transaction::type, std::string> type_map;
    type_map[transaction::type::Deposit] = "Deposit";
    type_map[transaction::type::Withdrawal] = "Withdrawal";

    os << "[" << t.amount << " " << type_map[t.transaction_type] << "]" << std::endl;

    return os;
  }

  std::string transaction::to_string() const
  {
    std::string result = "";
    result += "Amount:" + std::to_string(amount) + "\n";
    result += "Type: ";

    switch (transaction_type)
      {
      case type::Deposit:
        result += "Deposit";
        break;

      case type::Withdrawal:
        result += "Withdrawal";
        break;

      default:
        break;
      }

    return result;
  }
}
