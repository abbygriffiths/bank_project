#pragma once

#include <iostream>
#include <string>

namespace project
{
  struct date
  {
    int day, month, year;

    inline date(int d, int m, int y):
      day(d),
      month(m),
      year(y)
    {
    }

    inline std::string to_string() const
    {
      return std::to_string(day) + "/" +
        std::to_string(month) + "/" +
        std::to_string(year);
    }

    inline date operator=(const date& d)
    {
      return date(d.day, d.month, d.year);
    }

    friend std::ostream& operator<<(std::ostream& os, const date& d);
    friend std::istream& operator>>(std::istream& is, date& d);
  };

  inline std::ostream& operator<<(std::ostream& os, const date& d)
  {
    os << d.day << " " << d.month << " " << d.year;
    return os;
  }

  inline std::istream& operator>>(std::istream& is, date& d)
  {
    is >> d.day >> d.month >> d.year;
    return is;
  }
}
