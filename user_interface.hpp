#pragma once

#include <string>

namespace bank
{
  class bank_account;
}

namespace project
{
  class person;

  void show_main_menu();
  void show_login_menu();
  void show_sign_up_menu();
  void show_account_menu(const project::person& user);

  void print_separator();
  std::string get_password(const std::string& prompt = "Enter your password: ");

  void save_user(const project::person& user);
  void delete_user(const project::person& user);

  person search_user(const std::string& acc_number);

  int count_accounts();

  inline void getch() { std::system("read"); }

  inline constexpr int FAILURE = -1;
}
