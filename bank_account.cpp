#include <algorithm>
#include <iostream>

#include "bank_account.hpp"
#include "transaction.hpp"

namespace bank
{
  bank_account::bank_account():
    bank_account("")
  {
  }

  bank_account::bank_account(const std::string& password_,
                             double opening_balance /* = 0*/,
                             int offset /* = 0 */):
    account_number("AAA000" + std::to_string(bank_account::INITIAL_ACCOUNT_NUMBER + offset)),
    password(password_),
    account_balance(opening_balance)
  {
  }

  bank_account::bank_account(const bank_account& b_a):
    bank_account(b_a.password, b_a.account_balance)
  {
    account_number = b_a.account_number;
  }

  bank_account::bank_account(bank_account&& b_a)
  {
    swap(*this, b_a);
  }

  void bank_account::withdraw(double amt)
  {
    if (amt > 0 && amt <= account_balance)
      {
        account_balance -= amt;
        transactions.push_back(bank::transaction(amt, bank::transaction::type::Withdrawal));
      }
  }

  void bank_account::deposit(double amt)
  {
    if (amt > 0)
      {
        account_balance += amt;
        transactions.push_back(bank::transaction(amt, bank::transaction::type::Deposit));
      }
  }

  void bank_account::change_password(const std::string& password_)
  {
    password = password_;
  }

  void bank_account::print_details() const
  {
    std::cout << "Account number: " << account_number << std::endl
              << "Current account balance: " << account_balance << std::endl
              << "Transactions:" << std::endl;
    int n = transactions.size();

    for (int i = 0; i < n; i++)
      {
        std::cout << transactions[i] << std::endl;
      }
    std::cout << std::endl;
  }

  bool bank_account::verify_password(const std::string& password_) const
  {
    return password == password_;
  }

  bool bank_account::operator==(const bank_account& b_a) const
  {
    return account_number == b_a.account_number;
  }

  bank_account& bank_account::operator=(bank_account&& b_a)
  {
    swap(*this, b_a);
    return *this;
  }

  bank_account& bank_account::operator=(bank_account b_a)
  {
    swap(*this, b_a);
    return *this;
  }

  void swap(bank_account& b1, bank_account& b2)
  {
    using std::swap;

    swap(b1.account_number, b2.account_number);
    swap(b1.password, b2.password);
    swap(b1.account_balance, b2.account_balance);
    swap(b1.transactions, b2.transactions);
  }

  std::istream& operator>>(std::istream& is, bank_account& b_a)
  {
    std::getline(is, b_a.account_number, '|');
    std::getline(is, b_a.password, '|');
    is >> b_a.account_balance;

    char ch;

    is >> ch;

    while (ch != '}')
      {
        is >> ch;
        if (ch == '[')
          {
            transaction t;
            is >> t;
            b_a.transactions.push_back(t);
          }
        else
          {
            continue;
          }
      }
    return is;
  }

  std::ostream& operator<<(std::ostream& os, const bank_account& b_a)
  {
    os << b_a.account_number << "|"
       << b_a.password << "|"
       << b_a.account_balance << std::endl;

    os << "{";

    for (const transaction& t : b_a.transactions)
      {
        os << t << std::endl;
      }

    os << "}";

    return os;
  }
}
