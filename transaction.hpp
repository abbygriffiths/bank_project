#pragma once

#include <iostream>
#include <string>

namespace bank
{
  class transaction
  {
  public:
    enum class type
      {
        Deposit,
        Withdrawal,
        Default
      };

    transaction();
    transaction(double amt, type type_);

    std::string to_string() const;

    friend std::istream& operator >>(std::istream& is, transaction& t);
    friend std::ostream& operator<<(std::ostream& os, const transaction& t);

  private:
    double amount;
    type transaction_type;
  };
}
