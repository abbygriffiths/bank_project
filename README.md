#Bank Project
====

Description
----
This project is an implementation of a banking system.
Currency is implemented as floating point numbers, which means this should NOT be used in a real world
Provides features to customers such as withdrawal, deposits, interest and has minimum balance penalties

Contributors
----
- Abirbhav Goswami
- Abdullah Khan
- Atharva Mule

License
----
AGPL License (Self-developed)

Frameworks Used
----
- C++1z Standard Library

